
import React, { Component } from 'react';
import './App.css';
import ForecastContainer from './components/ForecastContainer';

class App extends Component {
  render() {
    return (
      <div className="App">
        <ForecastContainer />
      </div>
    );
  }
}

export default App;