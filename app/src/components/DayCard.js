import React from "react";
import moment from "moment";
import {
  faCloud,
  faSun,
  faMoon,
  faCloudMoon,
  faCloudSun,
  faCloudRain,
  faSnowflake,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const DayCard = ({ data }) => {
  const weekday = data.time * 1000;
  const faIcon = {
    "clear-day": faSun,
    "clear-night": faMoon,
    "partly-cloudy-day": faCloudSun,
    "partly-cloudy-night": faCloudMoon,
    cloudy: faCloud,
    snow: faSnowflake,
    rain: faCloudRain,
  };

  return (
    <div className="col-sm-2">
      <div className="card">
        <h3 className="card-title">{moment(weekday).format("dddd")}</h3>

        <p className="text-muted">
          {moment(weekday).format("MMMM Do")}
          <FontAwesomeIcon icon={faIcon[data.icon]} size="6x" />
        </p>
        <div className="card-body">
          <h5 className="card-title">
            {Math.round(data.apparentTemperatureMax)} °F
          </h5>
          <p className="card-text">
            <small className="text-muted">{data.summary}</small>
          </p>
        </div>
        <div className="card-footer bg-transparent"></div>
      </div>
    </div>
  );
};

export default DayCard;
