import React from "react";
import DayCard from "./DayCard";
import {
  faCloud,
  faSun,
  faMoon,
  faCloudMoon,
  faCloudSun,
  faCloudRain,
  faSnowflake,
  faSearch,
  faSearchLocation,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
// import data from "../forecast.json";

class ForecastContainer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      currently: [],
      daily: [],
      message: "",
      latitude: "",
      longitude: "",
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    this.setState({ [event.target.name]: event.target.value });
  }

  handleSubmit(event) {
    if (this.state.latitude && this.state.longitude) this.getForecast();
    event.preventDefault();
  }

  faIcon = {
    "clear-day": faSun,
    "clear-night": faMoon,
    "partly-cloudy-day": faCloudSun,
    "partly-cloudy-night": faCloudMoon,
    cloudy: faCloud,
    snow: faSnowflake,
    rain: faCloudRain,
  };

  getForecast = () => {
    fetch(
      `http://localhost:8080/api/forecast/${this.state.latitude},${this.state.longitude}`
    )
      .then((res) => res.json())
      .then((data) => {
        this.setState(
          {
            data,
            daily: data.daily.data,
            currently: data.currently,
          },
          () => console.log("data", this.state)
        );
      });
  };

  geoLocation = () => {
    const success = (position) => {
      const latitude = position.coords.latitude;
      const longitude = position.coords.longitude;

      this.setState({ latitude, longitude, message: "" });
      this.getForecast();
    };

    const error = () => {
      this.setState({ message: "Unable to retrieve your location" });
    };

    if (!navigator.geolocation) {
      this.setState({
        message: "Geolocation is not supported by your browser",
      });
    } else {
      this.setState({ message: "Locating…" });
      navigator.geolocation.getCurrentPosition(success, error);
    }
  };

  getDailyCards = () => {
    return this.state.daily.map((data, index) => (
      <DayCard data={data} key={index} />
    ));
  };

  render() {
    return (
      <div className="container">
        {this.state.message ? (
          <div className="alert alert-primary alert-float" role="alert">
            {this.state.message}
          </div>
        ) : (
          ""
        )}

        <div className="jumbotron left mt-5">
          <h1 className="display-4">Weather Forecast</h1>

          <div className="ml-2">
            Example Coordinates:
            <code>Los Angeles: 34.0207305, -118.6919155</code>
            <code>New York: 40.6976637, -74.119764</code>
            <code>Chicago: 41.8339037, -87.8720468</code>
          </div>

          {/* latitude & longitude entry */}

          <form className="form-inline pt-5" onSubmit={this.handleSubmit}>
            <div className="form-group mb-2">
              <label className="col-sm-2 col-form-label">Latitude</label>
              <div className="col-sm-10">
                <input
                  type="number"
                  name="latitude"
                  className="form-control"
                  placeholder="33.61669"
                  value={this.state.latitude}
                  onChange={this.handleChange}
                />
              </div>
            </div>
            <div className="form-group mb-2">
              <label className="col-sm-2 col-form-label">Longitude</label>
              <div className="col-sm-10">
                <input
                  type="number"
                  name="longitude"
                  className="form-control"
                  placeholder="-117.6082659"
                  value={this.state.longitude}
                  onChange={this.handleChange}
                />
              </div>
            </div>

            <button
              type="submit"
              className="btn btn-primary mb-2 mr-2"
              //   onClick={() => this.geoLocation()}
            >
              <FontAwesomeIcon icon={faSearch} size="1x" className="mr-2" />
              Search
            </button>

            <button
              type="button"
              className="btn btn-primary mb-2"
              onClick={this.geoLocation}
            >
              <FontAwesomeIcon
                icon={faSearchLocation}
                size="1x"
                className="mr-2"
              />
              Get My Location
            </button>
          </form>

          {/* latitude & longitude results */}
          {this.state.data.timezone ? (
            <>
              <p className="lead">Time Zone: {this.state.data.timezone}</p>
              <FontAwesomeIcon
                icon={this.faIcon[this.state.currently.icon]}
                size="5x"
              />
              <div className="currentTemp">
                {Math.round(this.state.currently.apparentTemperature)} °F
              </div>
              <p className="lead">{this.state.currently.summary}</p>
              <table className="table">
                <tbody>
                  <tr>
                    <td>
                      Cloud Cover:
                      <span className="badge badge-light">
                        {this.state.currently.cloudCover} %
                      </span>
                    </td>
                    <td>
                      Dew Point:
                      <span className="badge badge-light">
                        {this.state.currently.dewPoint} °F
                      </span>
                    </td>
                    <td>
                      Humidity:
                      <span className="badge badge-light">
                        {this.state.currently.humidity}
                      </span>
                    </td>

                    <td>
                      Ozone:
                      <span className="badge badge-light">
                        {this.state.currently.ozone} DU
                      </span>
                    </td>
                    <td>
                      Pressure:
                      <span className="badge badge-light">
                        {this.state.currently.pressure} hPa
                      </span>
                    </td>
                    <td>
                      Nearest Storm Bearing:
                      <span className="badge badge-light">
                        {this.state.currently.nearestStormBearing} °
                      </span>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      UV Index:
                      <span className="badge badge-light">
                        {this.state.currently.uvIndex || "N/A"}
                      </span>
                    </td>
                    <td>
                      Visibility:
                      <span className="badge badge-light">
                        {this.state.currently.visibility} miles
                      </span>
                    </td>
                    <td>
                      Wind Bearing:
                      <span className="badge badge-light">
                        {this.state.currently.windBearing} °
                      </span>
                    </td>
                    <td>
                      Wind Gust:
                      <span className="badge badge-light">
                        {this.state.currently.windGust} mph
                      </span>
                    </td>
                    <td>
                      Wind Speed:
                      <span className="badge badge-light">
                        {this.state.currently.windSpeed} mph
                      </span>
                    </td>
                  </tr>
                </tbody>
              </table>
              {/* <p className="lead">
                <a
                  className="btn btn-primary btn-lg"
                  href="#"
                  role="button"
                  onClick={this.geoLocation}
                >
                  Get My Location
                </a>
              </p> */}
            </>
          ) : (
            ""
          )}
        </div>
        <div className="cards-container">
          <div className="d-flex flex-row flex-nowrap">
            {this.getDailyCards()}
          </div>
        </div>
      </div>
    );
  }
}

export default ForecastContainer;
