package darksky

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strings"

	forecast "./forecast"

	"github.com/gorilla/mux"
)

// Hello get test call
func Hello(res http.ResponseWriter, req *http.Request) {
	vars := mux.Vars(req)
	fullName := vars["fullName"]
	fmt.Fprint(res, "Hello "+fullName+", from DarkSky!")
}

// Forecast get weather
func Forecast(res http.ResponseWriter, req *http.Request) {
	// allow CORS
	res.Header().Set("Access-Control-Allow-Origin", "*")
	res.Header().Set("Access-Control-Allow-Headers", "Content-Type")

	vars := mux.Vars(req)
	coord := strings.Split(vars["coord"], ",")
	// fmt.Fprint(res, "coord "+coord)
	// fmt.Fprint(res, "latitude "+coord[0]+" & longitude "+coord[1])
	// fmt.Println(coord[0], coord[1])

	keybytes, err := ioutil.ReadFile("./api/forecast/api_key.txt")
	if err != nil {
		log.Fatal(err)
	}
	key := string(keybytes)
	key = strings.TrimSpace(key)

	lat := coord[0]
	long := coord[1]

	f, err := forecast.Get(key, lat, long, "now", forecast.US, forecast.English)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("%s: %s\n", f.Timezone, f.Currently.Summary)
	fmt.Printf("humidity: %.2f\n", f.Currently.Humidity)
	fmt.Printf("temperature: %.2f Fahrenheit\n", f.Currently.Temperature)
	fmt.Printf("wind speed: %.2f\n", f.Currently.WindSpeed)

	json.NewEncoder(res).Encode(f)
}
