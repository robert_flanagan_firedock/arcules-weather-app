package main

import (
	"fmt"
	"net/http"

	darksky "./api"
	"github.com/gorilla/mux"
)

func main() {
	router := mux.NewRouter()

	// api router
	api := router.PathPrefix("/api/").Subrouter()

	api.HandleFunc("/hello/{fullName}", darksky.Hello).Methods("GET")
	api.HandleFunc("/forecast/{coord}", darksky.Forecast).Methods("GET")

	// catch-all: serve react app entry-point (index.html).
	router.PathPrefix("/").Handler(http.StripPrefix("/", http.FileServer(http.Dir("app/build"))))

	err := http.ListenAndServe(":8080", router)
	if err != nil {
		fmt.Println(err)
	}
}
